﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forex.DAL;
using Forex.Models;
using Microsoft.AspNetCore.Mvc;

namespace Forex.Controllers
{

    [Route("api/[controller]/[action]")]
    public class MoneyController : Controller
    {
        private AppDbContext _appDbContext;

        public MoneyController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }



        [HttpPut]
        public IActionResult Srednia(string model)
        {
            

            return Ok();
        }


        [HttpPut]
        public IActionResult Wczytaj (string model)
        {
            List<MonetModel> monetModel = new List<MonetModel>();

            var m = model.Split(" ");

            foreach (var item in m)
            {
                var r = item.Split(";");
                MonetModel v= new MonetModel();
                v.Dolar = float.Parse(r[1]);
                v.Euro = float.Parse(r[2]);
                v.Frank = float.Parse(r[3]);
                v.Funt = float.Parse(r[4]);
                
                monetModel.Add(v);
                _appDbContext.Dane.Add(v);
                _appDbContext.SaveChanges();
            }

            return Ok();
        }
    }
}