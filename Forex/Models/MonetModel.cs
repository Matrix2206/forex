﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forex.Models
{
    public class MonetModel
    {
        public int Id { get; set; }
        public float Data { get; set; }
        public float Dolar { get; set; }
        public float Euro { get; set; }
        public float Funt { get; set; }
        public float Frank { get; set; }
    }
}
